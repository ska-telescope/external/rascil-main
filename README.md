
# Radio Astronomy Simulation, Calibration and Imaging Library

The Radio Astronomy Simulation, Calibration and Imaging Library
expresses radio interferometry calibration and imaging algorithms in
python and numpy. The interfaces all operate with familiar data structures
such as image, visibility table, gaintable, etc. The python source
code is directly accessible from these documentation pages: see the
source link in the top right corner.

[![Documentation Status](https://readthedocs.org/projects/rascil/badge/?version=latest)](https://developer.skao.int/projects/rascil/en/latest/?badge=latest)


The [Documentation](https://developer.skao.int/projects/rascil/en/latest/index.html) includes usage 
examples, API, and installation directions.

RASCIL CI/CD occurs on  [Gitlab](https://gitlab.com/ska-telescope/external/rascil-main)

The repository contains the [ska-cicd-makefile](https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile)
submodule, therefor clone using the following command:

    git clone https://gitlab.com/ska-telescope/external/rascil-main.git --recurse-submodules

## Contributing to this repository

[Black](https://github.com/psf/black), [isort](https://pycqa.github.io/isort/),
and various linting tools are used to keep the Python code in good shape.
Please check that your code follows the formatting rules before committing it
to the repository. You can apply Black and isort to the code with:

```bash
make python-format
```

and you can run the linting checks locally using:

```bash
make python-lint
```

The linting job in the CI pipeline does the same checks, and it will fail if
the code does not pass all of them.


