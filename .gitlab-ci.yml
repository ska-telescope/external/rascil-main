image: $SKA_K8S_TOOLS_BUILD_DEPLOY

variables:
  MPLBACKEND: "agg"
  GIT_SUBMODULE_STRATEGY: recursive
  
workflow:
  rules:
    - if: $CI_COMMIT_BRANCH
    - if: $CI_COMMIT_TAG

default:
  tags:
    - ska-default

stages:
  - lint
  - build
  - test
  - clean
  - publish
  - scan
  - prepost

# Build the data file only if on the master
data:
  stage: build
  before_script:
    - if [[ ! -d public ]]; then
        mkdir -p public;
      fi;
  script:
    - tar -zcf rascil_data.tgz data
    - mv rascil_data.tgz ./public
  artifacts:
    paths:
      - public
    expire_in: 6 months
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'

docker-build:
  stage: build
  variables:
    GIT_VERSION: $CI_COMMIT_SHORT_SHA
    IMAGE_PREFIX: $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME
  before_script:
    - cd docker
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - make build_push_all_git
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'

docker-test:
  stage: test
  variables:
    GIT_VERSION: $CI_COMMIT_SHORT_SHA
    IMAGE_PREFIX: $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME
  before_script:
    - cd docker
  script:
    - make pull_all_default
    - make test_all
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'

# Publish docker:<version> (release) to Central Artefact Repository if this is a tagged build of the master
docker-release:
  stage: publish
  variables:
    GIT_VERSION: $CI_COMMIT_SHORT_SHA
    CI_COMMIT_BRANCH: master
  before_script:
    - cp dist/*whl docker/rascil-base
    - cd docker
    - echo $CAR_OCI_REGISTRY_PASSWORD | docker login --username $CAR_OCI_REGISTRY_USERNAME --password-stdin $CAR_OCI_REGISTRY_HOST
  script:
    - make build_all_latest
    - make test_all
    - make push_all_release
  rules:
    - if: '$CI_COMMIT_TAG'

prepare-ci-metrics:
  stage: prepost
  before_script:
    - pip install coverage
  script:
    - mkdir -p build/reports
    - coverage combine coverage_* && coverage xml && coverage html -d coverage && coverage report
    - test -a coverage.xml && mv coverage.xml ./build/reports/code-coverage.xml
    - python3 util/xmlcombine.py unit-tests-dask.xml unit-tests-other.xml > unit-tests.xml # unit-tests-gpu.xml
    - test -a unit-tests.xml && mv unit-tests.xml ./build/reports/unit-tests.xml
  artifacts:
    paths:
      - coverage
      - ./build
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'

include:
  - project: 'ska-telescope/templates-repository'
    file:
      - 'gitlab-ci/includes/python.gitlab-ci.yml'
      # only use docs-build because we don't need to
      # build pages which is part of the full template
      - 'gitlab-ci/includes/docs-build.gitlab-ci.yml'
      - 'gitlab-ci/includes/finaliser.gitlab-ci.yml'

  - local: '.gitlab/schedules.yaml'
  - local: '.gitlab/ci-tests.yaml'
  - local: '.gitlab/cluster-test.yaml'

python-lint:
  before_script:
    - poetry config virtualenvs.in-project true
    - poetry config virtualenvs.create $POETRY_CONFIG_VIRTUALENVS_CREATE
    - poetry install --only=dev
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule"'

# do not run the following jobs on schedule
docs-build:
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'

python-build-for-development:
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule" && ($CI_COMMIT_TAG == "" || $CI_COMMIT_TAG == null)'

python-publish-to-gitlab:
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule"'
