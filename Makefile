include .make/base.mk
include .make/python.mk
include .make/dependencies.mk
include make/test.mk
include make/helm-dask.mk
include make/cluster-test.mk

-include PrivateRules.mak

PROJECT_NAME = rascil-main

PYTHON ?= python3
PYLINT ?= pylint
MAKE_DBG ?= ""
TESTS ?= tests/
FLAKE ?= flake8
NAME = rascil
IMG ?= $(NAME)
TAG ?= ubuntu18.04
DOCKER_IMAGE = $(IMG):$(TAG)
WORKER_RASCIL_DATA ?= /rascil/data
CURRENT_DIR = $(shell pwd)
JUPYTER_PASSWORD ?= changeme

CRED=\033[0;31m
CBLUE=\033[0;34m
CEND=\033[0m
LINE:=$(shell printf '=%.0s' {1..70})

# Set default docker registry user.
ifeq ($(strip $(DOCKER_REGISTRY_USER)),)
	DOCKER_REGISTRY_USER=ci-cd
endif

ifeq ($(strip $(DOCKER_REGISTRY_HOST)),)
	DOCKER_REGISTRY_HOST=artefact.skao.int
endif

# RASCIL data directory usualy found in ./data
RASCIL_DATA = $(CURRENT_DIR)/data

GIT_ROOT_DIR=$(shell git rev-parse --show-toplevel)
GIT_VERSION ?= $(shell git rev-parse --verify --short=8 HEAD)
VERSION=$(shell awk -F= '/^__version__ = /{print $$2}' $(GIT_ROOT_DIR)/rascil/version.py | sed -r "s/\\\"//g" | tr -d " ")

RASCIL_FULL_IMG ?= registry.gitlab.com/ska-telescope/external/rascil-main/rascil-full
RASCIL_FULL_VERSION ?= $(VERSION)-$(GIT_VERSION)
RASCIL_IMG = $(RASCIL_FULL_IMG):$(RASCIL_FULL_VERSION)

# for the test-dask job and tests
HELM_TIMEOUT ?= 5m
HELM_RELEASE ?= test-rascil-dask
HELM_REPO ?= ska-helm
RASCIL_DASK_SCHEDULER ?= rascil-dask-scheduler.dp-orca:8786
TIME_TO_LIVE ?= 20  # [s] delete helm charts older than this in scheduled clean up
DASK_NAMESPACE ?= dp-orca-p
TEST_PVC ?= rascil-test-pvc

# for using the standard python make targets
PYTHON_SRC = rascil
PYTHON_LINE_LENGTH = 88  # default black line length

# E203 whitespace before ':' (black incompatible)
# W503 line break before binary operator (black incompatible)
# F403 'from .apps_parser import *' used; unable to detect undefined names
# F405 'rascil_imager' may be undefined, or defined from star imports: .apps_parser
PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=E203,W503,F403,F405

# E0401: import-error - ignore so CI doesn't need to install all requirements
# E1101: no-member - astropy.constants and HDUList
# W0612: unused-variable
PYTHON_SWITCHES_FOR_PYLINT = --disable=E0401,E1101

.DEFAULT_GOAL := help

# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) command to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

.PHONY: release-patch release-minor release-major docs-pre-build

release-patch: ## Make patch release
	bumpver update --patch -n

release-minor: ## Make minor release
	bumpver update --minor -n

release-major: ## Make major release
	bumpver update --major -n

# for both docs jobs we need to install all of the requirements
# for automodapi to work; by default the template only installs docs reqs
docs-pre-build:
	poetry install