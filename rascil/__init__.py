from . import phyconst, processing_components, workflows  # noqa=F401
from .processing_components.util.installation_checks import check_data_directory

check_data_directory(fatal=False)
