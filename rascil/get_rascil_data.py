"""
Script to download data needed for RASCIL
"""

import logging
import os
import subprocess
import tarfile
from urllib.request import urlretrieve

LOG = logging.getLogger("rascil-logger")
RASCIL = os.getenv("RASCIL")


def _get_rascil_data():
    """Download RASCIL data."""
    rascil_data = os.path.abspath(f"{RASCIL}/rascil_data")
    if not os.path.isdir(rascil_data):
        os.makedirs(rascil_data)

    LOG.info("Saving RASCIL data to %s", rascil_data)

    data_file = "rascil_data.tgz"
    source_path = f"https://ska-telescope.gitlab.io/external/rascil-main/{data_file}"
    dest_path = f"{rascil_data}/{data_file}"

    # downloading data
    urlretrieve(source_path, dest_path)

    # extract data
    data = tarfile.open(dest_path)
    data.extractall(rascil_data)
    data.close()

    # delete tar file
    os.remove(dest_path)


def _get_casa_data():
    """Download casacore measures data."""
    casa_dir = os.path.abspath(f"{RASCIL}/casacore_data")
    LOG.info("Saving casacore measures data to %s.", casa_dir)

    # add path to .casarc
    if not os.path.isdir(casa_dir):
        LOG.info("Create $HOME/.casarc")
        with open(f"{os.getenv('HOME')}/.casarc", "w") as casa:
            casa.write(f"measures.directory: {casa_dir}")

    # download data
    subprocess.run(
        ["rsync", "-avz", "rsync://casa-rsync.nrao.edu/casa-data/geodetic", casa_dir]
    )


def main():
    """Download data"""
    if not RASCIL:
        raise OSError("RASCIL environment variable is not set")

    _get_rascil_data()
    _get_casa_data()

    LOG.info("Data downloaded.")


if __name__ == "__main__":
    main()
