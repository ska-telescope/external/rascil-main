"""
Calibration of observations, using single Jones matricies or chains of Jones matrices.
"""

from .iterators import *  # noqa=F401
from .operations import *  # noqa=F401
