"""Unit tests for visibility scatter gather and extend MS file"""

import logging
import unittest

from ska_sdp_datamodels.visibility import (
    create_visibility_from_ms,
    extend_visibility_to_ms,
)

from rascil.processing_components.parameters import rascil_data_path, rascil_path

log = logging.getLogger("logger")

log.setLevel(logging.WARNING)


class TestExtendMS(unittest.TestCase):
    def setUp(self):
        try:
            import casacore  # noqa=F401

            self.casacore_available = True
        except ImportError:
            self.casacore_available = False

    def test_extend_ms(self):
        # Reading
        msfile = rascil_data_path("vis/xcasa.ms")
        msoutfile = rascil_path("test_results/test_extend_xcasa.ms")
        # remove temp file if exists
        import os
        import shutil

        if os.path.exists(msoutfile):
            shutil.rmtree(msoutfile, ignore_errors=False)
        # open an existent file
        bvis = create_visibility_from_ms(msfile)[0]
        bvis_list = [bv[1] for bv in bvis.groupby("time", squeeze=False)]
        for bvis in bvis_list:
            extend_visibility_to_ms(msoutfile, bvis)


if __name__ == "__main__":
    unittest.main()
