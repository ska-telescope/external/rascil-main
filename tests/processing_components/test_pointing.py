"""Tests for pointing.py"""

import numpy

from rascil.processing_components.simulation.pointing import _get_worldloc


def test_get_worldloc():
    """Test the _get_worldloc function"""

    azimuth_comp = numpy.deg2rad(160)
    elevation_com = numpy.deg2rad(40)
    az_comp = numpy.deg2rad(160.4)
    el_comp = numpy.deg2rad(40.2)
    pol = 0
    frequency = 50e6

    result = _get_worldloc(
        azimuth_comp, elevation_com, az_comp, el_comp, pol, frequency
    )
    numpy.testing.assert_allclose(
        result, [0.3055192516276098, 0.20068550896420456, 0, 50000000.0]
    )
