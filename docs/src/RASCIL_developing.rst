.. _rascil_developing:

Developing in RASCIL
********************

Use the SKA Python Coding Guidelines (http://developer.skatelescope.org/en/latest/development/python-codeguide.html).

Process
=======

- Use git to make a local clone of the Github respository::

   git clone https://gitlab.com/ska-telescope/external/rascil-main.git

- Make a branch. Use a descriptive name e.g. abc-123-feature_improved_gridding, abc-1231-bugfix_issue_666
  (Note that the branch name has to start with a Jira ticket ID)
- Make whatever changes are needed, including documentation.
- Always add appropriate test code in the tests directory.
- Consider adding to the examples area.
- Push the branch to gitlab. It will then be automatically built and tested on gitlab: https://gitlab.com/ska-telescope/external/rascil-main/-/pipelines
- Once it builds correctly, submit a merge request.


Design
======

The RASCIL has been designed in line with the following principles:

+ Data are held in Classes.
+ The Data Classes correspond to familiar concepts in radio astronomy packages e.g. visibility, gaintable, image.
+ The data members of the Data Classes are directly accessible by name e.g. .data, .name, .phasecentre.
+ Direct access to the data members is envisaged.
+ There are no methods attached to the data classes apart from variant constructors as needed.
+ Standalone, stateless functions are used for all processing.

Additions and changes should adhere to these principles.

Submitting code
===============

RASCIL is part of the SKA telescope organisation on GitLab. https://gitlab.com/ska-telescope/external/rascil-main.git.

We welcome merge requests submitted via GitLab.
[Black](https://github.com/psf/black), [isort](https://pycqa.github.io/isort/),
and various linting tools are used to keep the Python code in good shape.
Please check that your code follows the formatting rules before committing it
to the repository. You can apply Black and isort to the code with:

```bash
make python-format
```

and you can run the linting checks locally using:

```bash
make python-lint
```

The linting job in the CI pipeline does the same checks, and it will fail if
the code does not pass all of them.
