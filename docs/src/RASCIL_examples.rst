.. _rascil_examples:

Examples
========

Running notebooks
*****************

The best way to get familiar with RASCIL is via jupyter notebooks. For example::

   jupyter notebook examples/notebooks/imaging.ipynb

See the jupyter notebooks below:

   - `Imaging and deconvolution demonstration <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/examples/notebooks/imaging.ipynb>`_
   - `Simple demonstration of the use of Dask/rsexecute <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/examples/notebooks/simple-dask_rsexecute.ipynb>`_
   - `Bandpass calibration demonstration <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/examples/notebooks/bandpass-calibration.ipynb>`_
   - `Demonstrate visibility xarray format <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/examples/notebooks/demo_visibility_xarray.ipynb>`_

Some functions initially developed for the LOFAR telescope pipeline are made available in RASCIL. The following notebooks show how the functions are integrated.


   - `Deconvolution with Rascil and Radler <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/examples/notebooks/deconvolution.ipynb>`_
   - `Multi frequency deconvolution with Rascil and Radler <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/examples/notebooks/multi_frequency_deconvolution.ipynb>`_

In addition, there are other notebooks in examples/notebooks that are not built as part of this documentation.
In some cases it may be necessary to add the following to the notebook to locate the RASCIL data
:code:`%env RASCIL_DATA=~/rascil_data/data`

Running scripts
***************

Some example scripts are found in the directory examples/scripts.

   - `examples/scripts/imaging.py <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/examples/scripts/imaging.py>`_
   - `examples/scripts/primary_beam_zernikes.py <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/examples/scripts/primary_beam_zernikes.py>`_

SKA simulations
***************

* :ref:`genindex`
* :ref:`modindex`


