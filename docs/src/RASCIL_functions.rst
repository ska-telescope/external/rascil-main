.. _rascil_functions:

.. toctree::
   :maxdepth: 2

Functions
=========

NOTE: Some processing functions have been migrated to the
`ska-sdp-func-python repository <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python.git>`_,
please refer to the documentation there for information.
Functions on this page is an incomplete list.

Read existing Measurement Set
-----------------------------

Casacore must be installed for MS reading and writing:

* List contents of a MeasurementSet: :py:func:`rascil.processing_components.visibility.base.list_ms`
* Creates a list of Visibilities, one per FIELD_ID and DATA_DESC_ID: :py:func:`rascil.processing_components.visibility.base.create_visibility_from_ms`

Image
-----

* Image operations: :py:func:`rascil.processing_components.image.operations`
* Import from FITS: :py:func:`rascil.processing_components.image.operations.import_image_from_fits`
* Re-project coordinate system: :py:func:`rascil.processing_components.image.operations.reproject_image`
* Smooth image: :py:func:`rascil.processing_components.image.operations.smooth_image`
* FFT: :py:func:`rascil.processing_components.image.operations.fft_image_to_griddata_with_wcs`
* Remove continuum: :py:func:`rascil.processing_components.image.operations.remove_continuum_image`



