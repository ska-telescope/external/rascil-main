.. _rascil_install:

Installation
============

RASCIL can be run on a Linux or macOS machine or cluster of machines. At least 16GB physical
memory is necessary to run the full test suite. In general more memory is better. RASCIL uses Dask for
multi-processing and can make good use of multi-core and multi-node machines.

We recommend that you install RASCIL (and its dependencies) into a virtual environment.
RASCIL requires python 3.10 to 3.12.

Important information for MacOS users
+++++++++++++++++++++++++++++++++++++

MacOS users, before you get started installing any of the requirements, note the following:

- we recommend using `conda <https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html>`_
  for your environment
- first, you will need to install `python-casacore <https://pypi.org/project/python-casacore/>`_ separately with conda.
  (See :ref:`casacore_install` for more information)::

    conda install -c conda-forge python-casacore

- the following package do not support wheels for MacOS. If you want to install/work with them,
  follow their instructions (see also :ref:`astron_tools`):

    - aoflagger: https://aoflagger.readthedocs.io/en/latest/
    - dp3: https://dp3.readthedocs.io/en/latest/
    - radler: https://radler.readthedocs.io/en/latest/

Installation via pip
++++++++++++++++++++

If you just wish to run the package and do not intend to run simulations or tests, RASCIL can be installed using pip::

     pip3 install --index-url=https://artefact.skao.int/repository/pypi-all/simple rascil

This will download the latest stable version. At the moment, the wheel requires python 3.10 to 3.12.
We regularly update the package to comply with the latest python versions. Compatibility with more
recent versions will also be updated.

For simulations, you must add the data in a separate step::

    mkdir rascil_data
    cd rascil_data
    curl https://ska-telescope.gitlab.io/external/rascil-main/rascil_data.tgz -o rascil_data.tgz
    tar zxf rascil_data.tgz
    cd data
    export RASCIL_DATA=`pwd`

If you wish to run the RASCIL examples or tests, use one of the steps below.

Installation via docker
+++++++++++++++++++++++

If you are familiar with docker, an easy approach is to use that:

 .. toctree::
    :maxdepth: 1

    RASCIL_docker

Installation via git clone
++++++++++++++++++++++++++

Use of git clone is necessary if you wish to develop and possibly contribute to the RASCIL codebase.
Installation should be straightforward. We strongly recommend the use of a python virtual environment.

RASCIL requires python 3.10+.

The installation steps are:

- Use git to make a local clone of the Github repository::

   git clone https://gitlab.com/ska-telescope/external/rascil-main.git --recurse-submodules

  Note that RASCIL uses the `ska-cicd-makefile <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile>`_
  submodule, hence why you need to clone using the ``--recurse-submodules`` option.

- Change into that directory::

   cd rascil-main

- Install the required python packages and RASCIL package.
  We use `poetry <https://python-poetry.org/>`_ to manage requirements and we recommend using that for installation.
  If you already have a virtual environment set up, then turn off creating one by poetry::

    poetry config virtualenvs.create false

  Next, install the dependencies and the rascil python package::

    poetry install

- RASCIL makes use of a number of data files. These can be downloaded using Git LFS::

    pip install git-lfs
    git lfs install
    git-lfs pull

The data will be pulled into the ``data`` directory within the ``rascil-main`` git source directory.
If git-lfs is not already available, then lfs will not be recognised as a valid option for git in the second step.
In this case, git-lfs can be installed via :code:`sudo apt install git-lfs` or
from a `tar file <https://docs.github.com/en/github/managing-large-files/installing-git-large-file-storage>`_

- Put the following definitions in your .bashrc (.zshrc)::

    export RASCIL=/path/to/rascil
    export PYTHONPATH=$RASCIL:$PYTHONPATH

  Note: if you use a virtual environment, you will not need to update your PYTHONPATH.

Trouble-shooting
++++++++++++++++

Testing
^^^^^^^

Check your installation by running a subset of the tests::

   pip install pytest pytest-xdist
   py.test -n 4 tests/processing_components

Or the full set::

   py.test -n 4 tests

- Ensure that pip is up-to-date. If not, some strange install errors may occur.
- Check that the contents of the data directories have plausible contents.
  If gif-lfs has not been run successfully then the data files will just contain meta data,
  leading to strange run-time errors.
- There may be some dependencies that require either conda (or brew install on a mac).
- Ensure that you have created the directory `test_results` to store the test results.

.. _casacore_install:

Casacore installation
^^^^^^^^^^^^^^^^^^^^^

RASCIL requires python-casacore to be installed. This is included in the requirements for the RASCIL install and so
should be installed automatically via pip. In some cases there may not be a compatible binary install (wheel) available
via pip. If not, pip will download the source code of casacore and attempt a build from source. The most common failure
mode during the source build is that it cannot find the boost-python libraries. These can be installed via pip. If
errors like this occur, once rectified, re-installing python-casacore separately via pip may be required, prior to
re-commencing the RASCIL install.

Trouble-shooting problems with a source install can be difficult. If available, this can be avoided by using anaconda
(or miniconda) as the base for an environment. It supports python-casacore which can be installed with::

   conda install -c conda-forge python-casacore

It may also be possible to avoid some of the more difficult issues with building python-casacore by downloading CASA
prior to the RASCIL install.

On MacOS, we recommend using conda, and installing python-casacore with that prior to installing the other
RASCIL requirements. This proved to be the simplest way of getting casacore working without having to install
separate boost and casacore packages.

.. _astron_tools:

ASTRON software
^^^^^^^^^^^^^^^

The following packages have been integrated with RASCIL as alternative tools
for various radio astronomy processing functions. They can optionally be used.

- aoflagger for radio frequency interference flagging: https://aoflagger.readthedocs.io/en/latest/
- dp3 for data processing (LOFAR pipeline) : https://dp3.readthedocs.io/en/latest/
- radler for deconvolution: https://radler.readthedocs.io/en/latest/

These packages do not provide wheels for MacOS. You will need to build them from source
if you would like to use them. Please follow the guidance on the relevant websites.

RASCIL data in notebooks
^^^^^^^^^^^^^^^^^^^^^^^^

In some case the notebooks may not automatically find the RASCIL data directory, in which case explicitly setting the
RASCIL_DATA environment variable may be required: :code:`%env RASCIL_DATA=~/rascil_data/data`.
