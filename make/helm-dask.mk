# ----------------
# YAML definitions
# ----------------

define DASK_VALUES_YAML
image: $(RASCIL_IMG)
imagePullPolicy: IfNotPresent

scheduler:
  env:
    - name: "DASK_DISTRIBUTED__DASHBOARD__GRAPH_MAX_ITEMS"
      value: "1000000"
  tolerations:
  - key: "nvidia.com/gpu"
    operator: "Equal"
    value: "true"
    effect: "NoSchedule"

worker:
  replicas: 4
  volume:
    path: "/mnt/data"
    name: $(TEST_PVC)
  resources:
    limits:
      cpu: 13
      memory: 96Gi
    requests:
      cpu: 2
      memory: 16Gi
  tolerations:
  - key: "nvidia.com/gpu"
    operator: "Equal"
    value: "true"
    effect: "NoSchedule"

jupyter:
  enabled: false
endef

define PVC_YAML
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: $(TEST_PVC)
spec:
  storageClassName: nfss1
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: 5Gi
endef

export DASK_VALUES_YAML
export PVC_YAML

# --------------------------------------------------------------------
# Make commands for handling the dask cluster for the test-dask CI job
# --------------------------------------------------------------------

.PHONY: helm_repo install-dask-cluster uninstall-dask-cluster create-dask-pvc \
	delete-dask-pvc install-test-pod uninstall-test-pod clean-up-dask-cluster

helm_repo:
	helm repo add $(HELM_REPO) https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy-charts/-/raw/master/chart-repo
	helm repo update

install-dask-cluster: ## install a dask cluster in dp-orca ns from commit to run tests
	# the following assumes you have the ska-helm repo added with:
	# 	helm repo add ska-helm https://gitlab.com/ska-telescope/sdp/ska-sdp-helmdeploy-charts/-/raw/master/chart-repo
	@echo "$${DASK_VALUES_YAML}" | envsubst | helm upgrade --install $(HELM_RELEASE) $(HELM_REPO)/dask \
 	-n $(DASK_NAMESPACE) --wait --timeout=$(HELM_TIMEOUT) --values -

uninstall-dask-cluster:
	helm uninstall $(HELM_RELEASE) -n $(DASK_NAMESPACE) || true

create-dask-pvc:
	@echo "$${PVC_YAML}" | envsubst | kubectl -n $(DASK_NAMESPACE) apply -f -

delete-dask-pvc:
	@echo "$${PVC_YAML}" | envsubst | kubectl -n $(DASK_NAMESPACE) delete -f -

install-test-pod:
	helm upgrade --install $(HELM_RELEASE)-pod docker/kubernetes/charts/rascil-k8s-test-pod -n $(DASK_NAMESPACE) \
	--set name=$(HELM_RELEASE)-pod --set image.image=$(RASCIL_IMG) --wait --timeout=$(HELM_TIMEOUT)

uninstall-test-pod:
	helm uninstall $(HELM_RELEASE)-pod -n $(DASK_NAMESPACE) || true

clean-up-dask-cluster:
	DASK_NAMESPACE=$(DASK_NAMESPACE) TIME_TO_LIVE=$(TIME_TO_LIVE) bash make/clean_dask_cluster.sh