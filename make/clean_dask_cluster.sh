#!/bin/bash

charts=`helm ls -n $DASK_NAMESPACE -o json | jq -r --argjson timestamp $TIME_TO_LIVE '.[] | select (.updated | sub("\\\..*";"Z") | sub("\\\s";"T") | fromdate < now - $timestamp).name' | grep rascil`

if (( ${#charts} ));
then
echo "Charts to uninstall in namespace $DASK_NAMESPACE: \n $charts"
helm uninstall $charts -n $DASK_NAMESPACE
else
echo "There are no helm charts to uninstall in $DASK_NAMESPACE namespace"
fi