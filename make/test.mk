.PHONY: test test-dask test-dask-in-pod test-gpu

test:
	HOME=`pwd` py.test -n 8 \
	tests/apps tests/processing_components --verbose \
	--cov=rascil \
	--junitxml unit-tests-other.xml \
	--durations=30
	coverage html -d coverage

test-dask:
	HOME=`pwd` RASCIL_DASK_SCHEDULER=$(RASCIL_DASK_SCHEDULER) \
    py.test tests/workflows tests/apps_rsexecute --verbose \
	--cov=rascil \
	--junitxml unit-tests-dask.xml \
	--durations=30
	coverage html -d coverage

test-dask-in-pod: install-test-pod  # install-test-pod is in make/helm-dask.mk
	kubectl -n $(DASK_NAMESPACE) wait pod/$(HELM_RELEASE)-pod --for=condition=ready --timeout=300s
	kubectl -n $(DASK_NAMESPACE) cp tests/apps_rsexecute $(HELM_RELEASE)-pod:/rascil/tests
	kubectl -n $(DASK_NAMESPACE) cp tests/workflows $(HELM_RELEASE)-pod:/rascil/tests
	# requirements-test.txt does not exist by default;
	# it is created the before_script step of the CI job
	kubectl -n $(DASK_NAMESPACE) cp requirements-test.txt $(HELM_RELEASE)-pod:/rascil
	kubectl -n $(DASK_NAMESPACE) exec $(HELM_RELEASE)-pod -- bash -c \
	"mkdir -p /mnt/data/$(HELM_RELEASE)/test_results && \
	pip install -r requirements-test.txt && \
	RASCIL=/mnt/data/$(HELM_RELEASE) HOME=`pwd` RASCIL_DASK_SCHEDULER=$(RASCIL_DASK_SCHEDULER) \
    py.test tests --verbose \
	--cov=rascil \
	--junitxml unit-tests-dask.xml \
	--durations=30 && \
	coverage html -d coverage"

test-gpu:
	HOME=`pwd` py.test tests/wagg_gpu --verbose \
	--cov=rascil \
	--junitxml unit-tests-gpu.xml \
	--durations=30
	coverage html -d coverage
